# Delta Parse

A draft of what it might look to have macro derived delta updates for a struct.

- `delta_parse_types` is my look at what the macro might generate
- `delta_parse_macro` is mostly empty but will contain the actual proc macro
- the top level crate will export everything together. Probably should be a workspace instead of actual subdirs?
