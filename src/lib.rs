#[cfg(test)]
mod tests {
    #[test]
    fn doesnt_touch_old_fields() {
        use delta_parse_macro::DeltaParse;
        // #[derive(DeltaParse)]
        struct Foo {
            bar: i32,
            baz: f32,
        }

        let foo = Foo { bar: 3, baz: 0.0 };
        assert_eq!(foo.bar, 3);
        assert_eq!(foo.baz, 0.0);
    }
}
