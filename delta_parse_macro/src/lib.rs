extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};
use synstructure::decl_derive;

decl_derive!([DeltaParse] => gen_delta_parse);

fn gen_delta_parse(input: synstructure::Structure) -> proc_macro2::TokenStream {
    input.each(|binding_info| {
        println!("{:#?}", binding_info);
        ""
    })
}
