#[derive(Debug)]
// #[serde(untagged)]
pub enum TryParse<T> {
    Parsed(T),
    // TODO: I'm sure this doesn't need to be specialised to serde_json. Perhaps any Deserializer?
    Unparsed(serde_json::Value, Option<serde_json::Error>),
    NotPresent,
}
// Needs a #[serde(default)] attribute to deserialise to a NotPresent variant
impl<'de, T: serde::de::DeserializeOwned> serde::de::Deserialize<'de> for TryParse<T> {
    fn deserialize<D: serde::de::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let res = serde_json::Value::deserialize(deserializer)?;
        match res {
            serde_json::Value::Null => Ok(TryParse::Unparsed(serde_json::Value::Null, None)),
            _ => match T::deserialize(&res) {
                Ok(t) => Ok(TryParse::Parsed(t)),
                Err(e) => Ok(TryParse::Unparsed(res, Some(e))),
            },
        }
    }
}

#[derive(Clone, Debug)]
enum Error {}

trait HasDelta: Sized {
    type Delta: Sized;
    type Callbacks: Sized;

    fn mut_callbacks(
        self: &mut Self,
        delta: Self::Delta,
        callbacks: Self::Callbacks,
    ) -> Result<(), Error>;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn proof_of_concept() {
        struct Foo {
            bar: i32,
            baz: String,
            bat: f32,
        }
        // Generated struct
        struct FooDelta {
            bar_delta: TryParse<i32>,
            baz_delta: TryParse<String>,
            bat_delta: TryParse<f32>,
        }

        // Generated?
        struct FooCallbacks {
            bar_callback:
                Box<dyn Fn(&mut i32, Result<i32, (serde_json::Value, Option<serde_json::Error>)>)>,
            baz_callback: Box<
                dyn Fn(&mut String, Result<String, (serde_json::Value, Option<serde_json::Error>)>),
            >,
            bat_callback:
                Box<dyn Fn(&mut f32, Result<f32, (serde_json::Value, Option<serde_json::Error>)>)>,
        }

        impl HasDelta for Foo {
            type Delta = FooDelta;
            type Callbacks = FooCallbacks;

            fn mut_callbacks(
                self: &mut Self,
                delta: Self::Delta,
                mut_callbacks: Self::Callbacks,
            ) -> Result<(), Error> {
                match delta.bar_delta {
                    TryParse::Parsed(bar) => {
                        (mut_callbacks.bar_callback)(&mut self.bar, Ok(bar));
                    }
                    TryParse::Unparsed(value, option_err) => {
                        (mut_callbacks.bar_callback)(&mut self.bar, Err((value, option_err)))
                    }
                    TryParse::NotPresent => (),
                }

                match delta.baz_delta {
                    TryParse::Parsed(baz) => {
                        (mut_callbacks.baz_callback)(&mut self.baz, Ok(baz));
                    }
                    TryParse::Unparsed(value, option_err) => {
                        (mut_callbacks.baz_callback)(&mut self.baz, Err((value, option_err)))
                    }
                    TryParse::NotPresent => (),
                }

                match delta.bat_delta {
                    TryParse::Parsed(bat) => {
                        (mut_callbacks.bat_callback)(&mut self.bat, Ok(bat));
                    }
                    TryParse::Unparsed(value, option_err) => {
                        (mut_callbacks.bat_callback)(&mut self.bat, Err((value, option_err)))
                    }
                    TryParse::NotPresent => (),
                }

                Ok(())
            }
        }

        // This is the original type
        let mut foo = Foo {
            bar: 3,
            baz: "baz_str".to_owned(),
            bat: 4.0,
        };

        // This will be parsed
        let foo_delta = FooDelta {
            bar_delta: TryParse::Unparsed(
                serde_json::from_str::<serde_json::Value>(r#""foo""#).unwrap(),
                None,
            ),
            baz_delta: TryParse::Parsed("baz_new_value".to_owned()),
            bat_delta: TryParse::NotPresent,
        };

        // This is defined in the reconciler
        let foo_callbacks = FooCallbacks {
            bar_callback: Box::new(|old_mut, new_result| match new_result {
                Ok(new_value) => *old_mut = new_value,
                _ => (),
            }),
            baz_callback: Box::new(|old_mut, new_result| match new_result {
                Ok(new_value) => *old_mut = new_value,
                _ => (),
            }),
            bat_callback: Box::new(|old_mut, new_result| match new_result {
                Ok(new_value) => *old_mut = new_value,
                _ => (),
            }),
        };

        foo.mut_callbacks(foo_delta, foo_callbacks).unwrap();
        assert_eq!(foo.bar, 3);
        assert_eq!(foo.baz, "baz_new_value");
        assert_eq!(foo.bat, 4.0);
    }
}
